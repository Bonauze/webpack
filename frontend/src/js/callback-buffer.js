const getUniqueFunctions = (items) => (
  items.reduce((uniqueItems, item) => {
    if (typeof window[item] !== 'function') return uniqueItems;

    const includesItem = uniqueItems.some((uniqueItem) => (
      uniqueItem.toString() === item.toString()
    ));

    return includesItem ? uniqueItems : [...uniqueItems, item];
  }, [])
);

export default class CallbackBuffer {
  constructor(items) {
    this.items = getUniqueFunctions(items);

    this.runFunctions();
  }

  push(...items) {
    this.items = getUniqueFunctions(items);
    this.runFunctions();
  }

  runFunctions() {
    this.items.forEach((item) => window[item].call());
  }
}
