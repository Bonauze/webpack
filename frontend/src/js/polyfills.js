export default () => {
  if (!Element.prototype.closest) {
    Element.prototype.closest = function (selector) {
      let node = this;

      while (node) {
        if (node.matches(selector)) {
          return node;
        }

        node = node.parentElement;
      }

      return null;
    };
  }


  if (Element && !Element.prototype.matches) {
    const proto = Element.prototype;

    proto.matches = proto.matchesSelector
                    || proto.mozMatchesSelector
                    || proto.msMatchesSelector
                    || proto.oMatchesSelector
                    || proto.webkitMatchesSelector;
  }

  [Element.prototype, CharacterData.prototype, DocumentType.prototype].forEach((item) => {
    if (Object.prototype.hasOwnProperty.call(item, 'remove')) {
      return;
    }

    Object.defineProperty(item, 'remove', {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function remove() {
        this.parentElement.removeChild(this);
      },
    });
  });


  if (typeof window.CustomEvent !== 'function') {
    const CustomEvent = (event, parameters) => {
      const newParameters = parameters || { bubbles: false, cancelable: false, detail: undefined };
      const createdEvent = document.createEvent('CustomEvent');

      createdEvent.initCustomEvent(
        event,
        newParameters.bubbles,
        newParameters.cancelable,
        newParameters.detail,
      );

      return createdEvent;
    };

    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
  }
};
