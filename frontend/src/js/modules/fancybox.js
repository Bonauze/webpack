import '@fancyapps/fancybox/dist/jquery.fancybox.min.css';
import '@fancyapps/fancybox/dist/jquery.fancybox.min';

const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('.js-fancybox')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    const $elements = $(wrapper).find('[data-fancybox]');
    const randomGroupName = Math.random().toString();


    $elements.each((index, $element) => {
      if (!($element.dataset.fancybox)) {
        $element.dataset.fancybox = randomGroupName;
      }
    });


    $elements.fancybox({
      loop: true,
      animationEffect: 'fade',
      transitionEffect: 'slide',
      buttons: ['close'],
    });
  });
};
