import inViewport from 'in-viewport';

const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('.js-lazy-load')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    const events = ['load', 'scroll', 'resize', 'lazyLoad'];


    const isVisible = () => (
      wrapper.offsetParent !== null && inViewport(wrapper, {
        offset: 300,
      })
    );

    const getSrc = () => {
      const { modernSrc, outdatedSrc } = wrapper.dataset;

      if (modernSrc && document.documentElement.classList.contains('webp')) {
        return modernSrc;
      }

      return outdatedSrc;
    };

    const showImage = (src) => {
      wrapper.removeAttribute('data-modern-src');
      wrapper.removeAttribute('data-outdated-src');

      if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
        wrapper.setAttribute('src', src);
      } else {
        wrapper.style.opacity = '0';
        wrapper.setAttribute('src', src);

        setTimeout(() => {
          wrapper.style.transition = '0.8s opacity';
        }, 100);
        setTimeout(() => {
          wrapper.style.opacity = 1;
        }, 200);
        setTimeout(() => {
          wrapper.removeAttribute('style');
        }, 2000);
      }
    };

    const uploadImage = () => {
      const image = new Image();
      const src = getSrc();

      image.onload = () => showImage(src);
      image.onerror = () => setTimeout(uploadImage, 4000);

      image.src = src;
    };

    const tryUploadImage = () => {
      if (isVisible()) {
        removeEventHandlers();
        uploadImage();
      }
    };

    const removeEventHandlers = () => {
      events.forEach((event) => window.removeEventListener(event, tryUploadImage));
    };

    const registerEventHandlers = () => {
      events.forEach((event) => window.addEventListener(event, tryUploadImage));
    };

    if (isVisible()) {
      uploadImage();
    } else {
      registerEventHandlers();
    }
  });
};
