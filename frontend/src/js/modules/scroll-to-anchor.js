const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('.js-scroll-to-anchor')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    wrapper.addEventListener('click', (event) => {
      event.preventDefault();

      $('html, body').animate({
        scrollTop: document.querySelector(wrapper.getAttribute('href')).offsetTop - document.querySelector('#topbar').offsetHeight,
      }, 600);
    });
  });
};
