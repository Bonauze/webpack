const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('input, textarea')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    const errorClass = wrapper.closest('.field--error');


    wrapper.addEventListener('input', () => {
      if (errorClass) {
        errorClass.classList.remove('field--error');
      }
    });
  });
};
