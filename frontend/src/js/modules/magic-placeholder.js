const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('.js-magic-placeholder')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    const entry = wrapper.parentElement.querySelector('input, textarea');

    const activePlaceholder = () => wrapper.classList.add('active');
    const deactivatePlaceholder = () => wrapper.classList.remove('active');


    if (entry.value || document.activeElement === entry) {
      activePlaceholder();
    }


    ['change', 'focusin'].forEach((eventName) => entry.addEventListener(eventName, activePlaceholder));


    entry.addEventListener('focusout', () => {
      if (!entry.value || (entry.getAttribute('type') === 'tel' && !entry.classList.contains('inputmask-complete'))) {
        deactivatePlaceholder();
      }
    });
  });
};
