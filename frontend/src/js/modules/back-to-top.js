const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('.js-back-to-top')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    let lastTimeout;


    const showOrHideBtn = () => {
      clearTimeout(lastTimeout);

      lastTimeout = setTimeout(() => {
        if ((document.documentElement.scrollTop || document.body.scrollTop) > window.innerHeight) {
          wrapper.classList.add('visible');
        } else {
          wrapper.classList.remove('visible');
        }
      }, 200);
    };


    window.addEventListener('scroll', showOrHideBtn);

    wrapper.addEventListener('click', (event) => {
      event.preventDefault();

      $('html, body').animate({
        scrollTop: document.documentElement.offsetTop,
      }, 800);
    });


    showOrHideBtn();
  });
};
