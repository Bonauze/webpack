import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick.min';

const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('.js-slider')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    const list = wrapper.querySelector('.js-slider-list');

    if (list.children.length <= 1) {
      return;
    }

    $(list).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      infinite: true,
      autoplaySpeed: 5000,
      speed: 600,
      arrows: true,
      dots: true,
      prevArrow: '<button type="button" class="arrow arrow--prev"></button>',
      nextArrow: '<button type="button" class="arrow arrow--next"></button>',
    });
  });
};
