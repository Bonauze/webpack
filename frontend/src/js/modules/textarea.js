const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('.js-textarea')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    let windowWidth = window.innerWidth;


    const changeHeight = () => {
      wrapper.style.removeProperty('height');

      const heightInCss = +(window.getComputedStyle(wrapper).height.replace(/[^0-9.]/g, ''));
      const { scrollHeight } = wrapper;

      if (heightInCss < scrollHeight) {
        wrapper.style.height = `${scrollHeight}px`;
      }
    };


    window.addEventListener('resize', () => {
      if (windowWidth !== window.innerWidth) {
        windowWidth = window.innerWidth;

        changeHeight();
      }
    });

    wrapper.addEventListener('input', changeHeight);


    changeHeight();
  });
};
