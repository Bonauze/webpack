const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('.js-tabs')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    const nav = wrapper.querySelector('.js-tabs-nav');
    const links = nav.querySelectorAll('a');
    const contents = wrapper.querySelector('.js-tabs-content').children;


    const addNumbersInNavigation = () => {
      Array.from(links).forEach((link, index) => {
        link.setAttribute('data-id', index.toString());
      });
    };

    const changeTab = (tabIndex) => {
      Array.from(links).forEach((link, index) => {
        if (index === tabIndex) {
          link.parentElement.classList.add('active');
        } else {
          link.parentElement.classList.remove('active');
        }
      });

      Array.from(contents).forEach((content, index) => {
        if (index === tabIndex) {
          content.style.display = 'block';
        } else {
          content.style.display = 'none';
        }
      });
    };


    Array.from(links).forEach((link) => {
      link.addEventListener('click', (event) => {
        event.preventDefault();

        if (link.parentElement.classList.contains('active')) return;

        changeTab(+link.dataset.id);
      });
    });


    addNumbersInNavigation();
    changeTab(+wrapper.dataset.activeTab || 0);
  });
};
