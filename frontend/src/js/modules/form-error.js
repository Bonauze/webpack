const moduleName = `formError`;

export default (() => {
    Array.from(document.querySelectorAll(`input, textarea, select`)).forEach((wrapper) => {
        if (wrapper.dataset[`${moduleName}Init`] === `true`) return;
        wrapper.dataset[`${moduleName}Init`] = `true`;



        const errorClass = wrapper.closest(`.form-error`);



        wrapper.addEventListener(`input`, () => {
            if (errorClass) {
                errorClass.classList.remove(`form-error`);
            }
        });



    });
})();