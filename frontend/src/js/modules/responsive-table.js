const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('.js-responsive-table')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    if (wrapper.querySelector('thead')) {
      const titles = Array.from(wrapper.querySelectorAll('thead th')).map((title) => title.innerText);

      Array.from(wrapper.querySelectorAll('tbody tr')).forEach((row) => {
        Array.from(row.querySelectorAll('td')).forEach((cell, index) => {
          if (titles[index] !== undefined) {
            cell.setAttribute('data-title', titles[index]);
          }
        });
      });
    } else {
      wrapper.classList.add('no-thead');
    }
  });
};
