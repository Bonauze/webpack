const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('.js-quick-search')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    const entry = wrapper.querySelector('.js-quick-search-entry');
    const url = entry.dataset.quickSearchUrl;


    const fillFormData = (formData, dataForSend, parentKey) => {
      let formDataKey;

      for (const key in dataForSend) {
        formDataKey = (parentKey === undefined ? key : `${parentKey}[${key}]`);

        if (typeof dataForSend[key] === 'object') {
          fillFormData(formData, dataForSend[key], formDataKey);
        } else {
          formData.append(formDataKey, dataForSend[key]);
        }
      }
    };

    const clearElements = () => {
      const elements = wrapper.querySelector('.quick-search');

      if (elements) elements.remove();
    };

    const renderElements = (elementsData) => {
      let elements = '';

      Array.from(elementsData).forEach((elementData) => {
        elements += `
          <li class="quick-search__item" data-href="${elementData.link}">
            <div class="quick-search__row">
              <div class="quick-search__col-1">
                <a href="${elementData.link}" class="quick-search__link"><img src="${elementData.image}" alt="" class="quick-search__image"></a>
              </div>
  
              <div class="quick-search__col-2">
                <a href="${elementData.link}" class="quick-search__name">${elementData.name}</a>
                <span class="quick-search__code">${elementData.code}</span>
              </div>
            </div>
          </li>
        `;
      });

      const html = `
        <div class="quick-search">
          <ul class="quick-search__list">${elements}</ul>
        </div>
      `;

      clearElements();

      wrapper.insertAdjacentHTML('beforeend', html);
    };

    const sendValueEntryByAjax = (entryValue) => {
      const formData = new FormData();
      const xhr = new XMLHttpRequest();
      const dataForSend = { q: entryValue };

      fillFormData(formData, dataForSend);

      xhr.open('POST', url);
      // xhr.open('GET', url);
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
          if (xhr.responseText) {
            renderElements(JSON.parse(xhr.responseText));
          } else {
            clearElements();
          }
        }
      };
      xhr.send(formData);
    };

    const showElements = () => {
      const elements = wrapper.querySelector('.quick-search');

      if (elements) elements.classList.remove('hidden');
    };

    const hideElements = () => {
      const elements = wrapper.querySelector('.quick-search');

      if (elements) elements.classList.add('hidden');
    };

    const shiftUpActiveClassInItem = (items, activeItem) => {
      if (!activeItem) {
        items[items.length - 1].classList.add('active');
      } else {
        activeItem.classList.remove('active');

        const prevElement = activeItem.prev();

        if (activeItem && prevElement) prevElement.classList.add('active');
      }
    };

    const shiftDownActiveClassInItem = (items, activeItem) => {
      if (!activeItem) {
        items[0].classList.add('active');
      } else {
        activeItem.classList.remove('active');

        const nextElement = activeItem.next();

        if (activeItem && nextElement) nextElement.classList.add('active');
      }
    };

    const removeActiveClassInItem = () => {
      const activeItem = wrapper.querySelector('.quick-search__item.active');

      if (activeItem) activeItem.classList.remove('active');
    };


    entry.addEventListener('input', () => {
      const entryValue = entry.value;

      if (!entryValue) {
        clearElements();
        return;
      }

      sendValueEntryByAjax(entryValue);
    });

    entry.addEventListener('focusin', showElements);

    entry.addEventListener('focusout', removeActiveClassInItem);

    entry.addEventListener('keydown', (event) => {
      const { keyCode } = event;
      const activeItem = wrapper.querySelector('.quick-search__item.active');

      if ((keyCode === 38 || keyCode === 40 || keyCode === 13) && activeItem) {
        event.preventDefault();

        const items = wrapper.querySelectorAll('.quick-search__item');
        const hrefActiveItem = activeItem ? activeItem.dataset.href : undefined;

        if (keyCode === 38) {
          shiftUpActiveClassInItem(items, activeItem);
        } else if (keyCode === 40) {
          shiftDownActiveClassInItem(items, activeItem);
        } else if (keyCode === 13 && activeItem) {
          window.location = hrefActiveItem;
        }
      }
    });

    document.addEventListener('click', (event) => {
      const element = event.target.closest('.js-quick-search');

      if (!element) hideElements();
    });

    document.addEventListener('keydown', (event) => {
      const elements = wrapper.querySelector('.quick-search');

      if (event.keyCode === 27 && elements) {
        event.preventDefault();

        hideElements();
      }
    });
  });
};
