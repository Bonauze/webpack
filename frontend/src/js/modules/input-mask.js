import Inputmask from 'inputmask';

const initializedElements = [];

export default () => {
  Array.from(document.querySelectorAll('[type="tel"]')).forEach((wrapper) => {
    if (initializedElements.includes(wrapper)) return;
    initializedElements.push(wrapper);


    const addClass = () => wrapper.classList.add('inputmask-complete');
    const removeClass = () => wrapper.classList.remove('inputmask-complete');


    wrapper.addEventListener('input', () => {
      if (wrapper.inputmask) {
        if (wrapper.inputmask.isComplete()) {
          addClass();
        } else {
          removeClass();
        }
      } else if (wrapper.value !== '') {
        addClass();
      } else {
        removeClass();
      }
    });


    new Inputmask({
      mask: '+7 (999) 999-99-99',
      placeholder: '_',
      clearIncomplete: true,
      showMaskOnHover: false,
    }).mask(wrapper);
  });
};
