import 'normalize.css';

import './sass/general/_mixins.scss';
import './sass/general/_variables.scss';
import './sass/general/base.scss';
import './sass/general/fonts.scss';

import './sass/blocks/page.scss';
import './sass/blocks/pop-up.scss';
import './sass/blocks/field.scss';
import './sass/blocks/back-to-top.scss';

import './sass/blocks/yac.scss';
