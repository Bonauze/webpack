import polyfills from './js/polyfills';

import fancybox from './js/modules/fancybox';
import fieldError from './js/modules/field-error.js';
import inputMask from './js/modules/input-mask';
import lazyLoad from './js/modules/lazy-load';
import magicPlaceholder from './js/modules/magic-placeholder';
import popUp from './js/modules/pop-up';
import quickSearch from './js/modules/quick-search';
import responsiveTable from './js/modules/responsive-table';
import scrollToAnchor from './js/modules/scroll-to-anchor';
import slickCarousel from './js/modules/slick-carousel';
import tabs from './js/modules/tabs';
import textarea from './js/modules/textarea';
import yandexMap from './js/modules/yandex-map';
import backToTop from './js/modules/back-to-top';

import CallbackBufferClass from './js/callback-buffer';

import './styles-entry';

polyfills();
fancybox();
fieldError();
inputMask();
lazyLoad();
popUp();
quickSearch();
magicPlaceholder();
responsiveTable();
scrollToAnchor();
slickCarousel();
tabs();
textarea();
yandexMap();
backToTop();

window.callbackBuffer = new CallbackBufferClass(window.callbackBuffer || []);
